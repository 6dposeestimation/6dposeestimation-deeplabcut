import json
import os
import pathlib
import sys
import time

import numpy as np
from dlclive import DLCLive, Processor
from filter.moving_average_filter import MovingAverageFilter
import cv2

from src.tools.draw_3D import draw_model, draw_lines, draw_rubiks_text


class LiveData:
    def __init__(self, model_path, object_path):
        dlc_proc = Processor()
        self.dlc_live = DLCLive(model_path, processor=dlc_proc)

        file = open(pathlib.Path(object_path), "r")
        data = json.load(file)
        all_model_points = []
        keypoint_names = []
        for coordinate in data["keypoints"]:
            keypoint_names.append(coordinate["name"])
            all_model_points.append(
                (coordinate["position"]["x"], coordinate["position"]["y"], coordinate["position"]["z"]))

        self.lines = []
        for bone in data["skeleton"]:
            self.lines.append((keypoint_names.index(bone["keypoint1"]), keypoint_names.index(bone["keypoint2"])))

        self.all_model_points = np.array(all_model_points)

        self.old_time = time.time()
        self.fps = MovingAverageFilter(10, 1)

        self.error_filter = MovingAverageFilter(20, 1)

        self.lastRVec = None
        self.lastTVec = None

    def initFrame(self, frame):
        self.dlc_live.init_inference(frame)

    def get_keypoints(self, frame, last_frame):
        image_points = []
        model_points = []
        pose = self.dlc_live.get_pose(frame)
        probabilities = [part[2] for part in pose]
        probabilities.sort(reverse=True)
        frame = frame.copy()
        for index, part in enumerate(pose):
            if part[2] > 0.1:
                frame = cv2.circle(frame, (int(part[0]), int(part[1])), radius=4, color=(0, 0, 0), thickness=2)
                image_points.append((int(part[0]), int(part[1])))
                model_points.append(self.all_model_points[index])
        image_points_frame = image_points.copy()
        model_points_frame = model_points.copy()
        # keep old keypoints not visible in this picture
        # for index, point in enumerate(last_frame[1]):
        #    if not any(point[0] == model_point[0] and point[1] == model_point[1] and point[2] == model_point[2] for
        #               model_point in model_points):
        #        image_points.append(last_frame[0][index])
        #        model_points.append(point)
        return image_points, model_points, image_points_frame, model_points_frame, frame

    def get_pose(self, model_points, image_points, camera_matrix, dist_coeffs):
        if len(image_points) >= 4:
            model_points = np.array(model_points)
            image_points = np.array(image_points, dtype="double")
            (success, rotation_vector, translation_vector, _) = cv2.solvePnPRansac(objectPoints=model_points,
                                                                                   imagePoints=image_points,
                                                                                   cameraMatrix=camera_matrix,
                                                                                   distCoeffs=dist_coeffs,
                                                                                   useExtrinsicGuess=True,
                                                                                   iterationsCount=500,
                                                                                   rvec=self.lastRVec,
                                                                                   tvec=self.lastTVec)
        else:
            success = False
            rotation_vector = None
            translation_vector = None
        return success, rotation_vector, translation_vector

    def get_model_points(self, model_points, rotation_vector, translation_vector, camera_matrix, dist_coeffs):
        projected_model_points, _ = cv2.projectPoints(self.all_model_points, rotation_vector,
                                                      translation_vector,
                                                      camera_matrix, dist_coeffs)
        projected_model_points_masked, _ = cv2.projectPoints(model_points, rotation_vector, translation_vector,
                                                             camera_matrix, dist_coeffs)
        return projected_model_points, projected_model_points_masked

    def pose_error(self, model_points, image_points, projected_model_points_masked, rotation_vector, translation_vector,
                   camera_matrix, dist_coeffs):
        pose_estimation_error = np.linalg.norm(
            np.subtract(image_points, projected_model_points_masked[:, 0, :]))
        pose_estimation_error = np.mean(pose_estimation_error)
        return pose_estimation_error

    def start_loop(self, folder_path=None, video_path=None, webcam_number=None):
        init = False
        if webcam_number is not None:
            cap = cv2.VideoCapture(webcam_number)

            # Check if the webcam is opened correctly
            if not cap.isOpened():
                raise IOError("Cannot open webcam")
        elif video_path is not None:
            cap = cv2.VideoCapture(video_path)
        elif folder_path is not None:
            if not os.path.isdir(folder_path + "/tracked"):
                os.mkdir(folder_path + "/tracked")
            image_files = [file for file in os.listdir(folder_path) if
                           file.endswith('.png') or file.endswith(".jpeg") or file.endswith(".jpg")]
        last_points = [[], []]
        frame_number = 0
        while True:
            image_path = None
            if folder_path is not None:
                if frame_number >= len(image_files):
                    exit(0)
                image_path = folder_path + "/tracked/" + image_files[frame_number]
                frame = cv2.imread(folder_path + "/" + image_files[frame_number])
                # time.sleep(1)
            else:
                _, frame = cap.read()
            if frame is None:
                exit(0)
            size = frame.shape
            frame_number += 1
            if not init:
                self.initFrame(frame)
                init = True
            image_points, model_points, image_points_frame, model_points_frame, frame = self.get_keypoints(frame,
                                                                                                           last_points)

            last_points = [image_points_frame, model_points_frame]

            image_points = np.array(image_points, dtype="double")

            # 3D model points.
            model_points = np.array(model_points)

            focal_length = size[1]
            center = (size[1] / 2, size[0] / 2)
            camera_matrix = np.array(
                [[focal_length, 0, center[0]],
                 [0, focal_length, center[1]],
                 [0, 0, 1]], dtype="double"
            )
            dist_coeffs = np.zeros((4, 1))

            success, rotation_vector, translation_vector = self.get_pose(model_points, image_points,
                                                                         camera_matrix, dist_coeffs)
            if success:
                if self.lastRVec is not None:
                    print([x1[0] - x2[0] for (x1, x2) in zip(rotation_vector, self.lastRVec)])
                    print(rotation_vector)
                    print(self.lastRVec)
                projected_model_points, projected_model_points_masked = self.get_model_points(model_points,
                                                                                              rotation_vector,
                                                                                              translation_vector,
                                                                                              camera_matrix,
                                                                                              dist_coeffs)

                pose_estimation_error = self.pose_error(model_points, image_points, projected_model_points_masked,
                                                        rotation_vector,
                                                        translation_vector,
                                                        camera_matrix, dist_coeffs)

                print("pose_estimation_error:", pose_estimation_error)
                self.error_filter.update([pose_estimation_error])
                for p in image_points:
                    cv2.circle(frame, (int(p[0]), int(p[1])), 3, (0, 0, 255), -1)
                error_average = self.error_filter.predict()[0]
                print("pose_estimation_average:", error_average)
                if pose_estimation_error > error_average:
                    print("ERROR")
                    projected_model_points, projected_model_points_masked = self.get_model_points(model_points,
                                                                                                  rotation_vector,
                                                                                                  translation_vector,
                                                                                                  camera_matrix,
                                                                                                  dist_coeffs)
                # frame = self.draw_axis(frame, rotation_vector, translation_vector, camera_matrix, dist_coeffs)
                frame = draw_model(frame, projected_model_points)
                frame = draw_lines(frame, projected_model_points, self.lines)
                frame = draw_rubiks_text(frame, projected_model_points, projected_model_points_masked)
                self.lastTVec = translation_vector
                self.lastRVec = rotation_vector
            cv2.imshow('Input', frame)
            if image_path is not None:
                image_path_split = image_path.split(".")
                image_path = '.'.join(image_path_split[:-1]) + "-tracked." + image_path_split[-1]
                cv2.imwrite(image_path, frame)
            new_time = time.time()
            self.fps.update([(1 / (new_time - self.old_time))])
            self.old_time = new_time
            cv2.setWindowTitle('Input', "FPS: " + str(int(self.fps.predict()[0])))
            c = cv2.waitKey(1)
            if c == 27:
                break
        cap.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Wrong arguments: python live_data.py <exported-model-folder> <object-data> <mode> <mode-argument>")
        exit(0)

    live_data = LiveData(sys.argv[1],
                         sys.argv[2])
    if sys.argv[3] == "webcam":
        live_data.start_loop(webcam_number=int(sys.argv[4]))
    elif sys.argv[3] == "folder":
        live_data.start_loop(folder_path=sys.argv[4])
    elif sys.argv[3] == "video":
        live_data.start_loop(video_path=sys.argv[4])
    else:
        print("Wrong arguments: python live_data.py <exported-model-folder> <object-data> <mode> <mode-argument>")
        exit(0)
