import numpy as np
from cv2 import cv2


def draw_axis(frame, rotation_vector, translation_vector, camera_matrix, dist_coeffs):
    (points2D, _) = cv2.projectPoints(
        np.array([(0.0, 0.0, 0.0), (5.0, 0.0, 0.0), (0.0, 5.0, 0.0), (0.0, 0.0, 5.0)]),
        rotation_vector,
        translation_vector, camera_matrix, dist_coeffs)
    frame = cv2.line(frame, (int(points2D[0][0][0]), int(points2D[0][0][1])),
                     (int(points2D[1][0][0]), int(points2D[1][0][1])), (255, 0, 0), 4)
    frame = cv2.line(frame, (int(points2D[0][0][0]), int(points2D[0][0][1])),
                     (int(points2D[2][0][0]), int(points2D[2][0][1])), (0, 255, 0), 4)
    frame = cv2.line(frame, (int(points2D[0][0][0]), int(points2D[0][0][1])),
                     (int(points2D[3][0][0]), int(points2D[3][0][1])), (0, 0, 255), 4)
    return frame


def draw_model(frame, projected_model_points):
    for i, p in enumerate(projected_model_points[:, 0]):
        if i == 120:
            frame = cv2.circle(frame, (int(p[0]), int(p[1])), 6, (255, 0, 0), -1)
        else:
            frame = cv2.circle(frame, (int(p[0]), int(p[1])), 3, (0, 255, 0), -1)
    return frame


def draw_rubiks_text(frame, projected_model_points, projected_model_points_masked):
    for i, p in enumerate(projected_model_points[:, 0]):
        if i == 8 and p in projected_model_points_masked[:, 0]:
            frame = cv2.putText(frame, "Orange", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (0, 0, 0), 6, cv2.LINE_AA)
            frame = cv2.putText(frame, "Orange", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (255, 255, 255), 2, cv2.LINE_AA)
        if i == 9 and p in projected_model_points_masked[:, 0]:
            frame = cv2.putText(frame, "Green", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (0, 0, 0), 6, cv2.LINE_AA)
            frame = cv2.putText(frame, "Green", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (255, 255, 255), 2, cv2.LINE_AA)
        if i == 10 and p in projected_model_points_masked[:, 0]:
            frame = cv2.putText(frame, "Red", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (0, 0, 0), 6, cv2.LINE_AA)
            frame = cv2.putText(frame, "Red", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (255, 255, 255), 2, cv2.LINE_AA)
        if i == 11 and p in projected_model_points_masked[:, 0]:
            frame = cv2.putText(frame, "Blue", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (0, 0, 0), 6, cv2.LINE_AA)
            frame = cv2.putText(frame, "Blue", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (255, 255, 255), 2, cv2.LINE_AA)
        if i == 12 and p in projected_model_points_masked[:, 0]:
            frame = cv2.putText(frame, "White", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (0, 0, 0), 6, cv2.LINE_AA)
            frame = cv2.putText(frame, "White", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (255, 255, 255), 2, cv2.LINE_AA)
        if i == 13 and p in projected_model_points_masked[:, 0]:
            frame = cv2.putText(frame, "Yellow", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (0, 0, 0), 6, cv2.LINE_AA)
            frame = cv2.putText(frame, "Yellow", (int(p[0]), int(p[1])), cv2.FONT_HERSHEY_COMPLEX, 0.5,
                                (255, 255, 255), 2, cv2.LINE_AA)
    return frame


def draw_lines(frame, projected_model_points, lines):
    for line in lines:
        frame = cv2.line(frame, (
            int(projected_model_points[line[0]][0][0]), int(projected_model_points[line[0]][0][1])),
                         (int(projected_model_points[line[1]][0][0]),
                          int(projected_model_points[line[1]][0][1])), (255, 255, 255), 1)
    return frame
