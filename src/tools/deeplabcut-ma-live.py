import os
from pathlib import Path

import cv2
from deeplabcut.pose_estimation_tensorflow.config import load_config
from deeplabcut.pose_estimation_tensorflow.core import predict
from deeplabcut.pose_estimation_tensorflow.core import predict_multianimal
from deeplabcut.utils import auxiliaryfunctions
from skimage.util import img_as_ubyte
from skimage.color import rgba2rgb
import numpy as np


class DeepLabCutMA:
    def __init__(self, cfg_path):
        self.cfg_path = cfg_path
        self.is_init = False

    def init_frame(self, frame):
        if self.is_init:
            return
        self.is_init = True
        cfg = auxiliaryfunctions.read_config(self.cfg_path)
        if "TF_CUDNN_USE_AUTOTUNE" in os.environ:
            del os.environ["TF_CUDNN_USE_AUTOTUNE"]  # was potentially set during training

        trainFraction = cfg["TrainingFraction"][0]
        iteration = cfg["iteration"]

        modelfolder = os.path.join(
            cfg["project_path"],
            str(
                auxiliaryfunctions.GetModelFolder(
                    trainFraction, 1, cfg, modelprefix=""
                )
            ),
        )
        path_test_config = Path(modelfolder) / "test" / "pose_cfg.yaml"
        try:
            self.dlc_cfg = load_config(str(path_test_config))
        except FileNotFoundError:
            raise FileNotFoundError(
                "It seems the model for iteration %s and shuffle %s and trainFraction %s does not exist."
                % (iteration, 1, trainFraction)
            )
        # Check which snapshots are available and sort them by # iterations
        try:
            Snapshots = np.array(
                [
                    fn.split(".")[0]
                    for fn in os.listdir(os.path.join(modelfolder, "train"))
                    if "index" in fn
                ]
            )
        except FileNotFoundError:
            raise FileNotFoundError(
                "Snapshots not found! It seems the dataset for shuffle %s has not been trained/does not exist.\n Be sure you also have the intended iteration number set.\n Please train it before using it to analyze videos.\n Use the function 'train_network' to train the network for shuffle %s."
                % (1, 1)
            )

        if cfg["snapshotindex"] == "all":
            print(
                "Snapshotindex is set to 'all' in the config.yaml file. Running video analysis with all snapshots is very costly! Use the function 'evaluate_network' to choose the best the snapshot. For now, changing snapshot index to -1!"
            )
            snapshotindex = -1
        else:
            snapshotindex = cfg["snapshotindex"]

        increasing_indices = np.argsort([int(m.split("-")[1]) for m in Snapshots])
        Snapshots = Snapshots[increasing_indices]

        print("Using %s" % Snapshots[snapshotindex], "for model", modelfolder)

        ##################################################
        # Load and setup CNN part detector
        ##################################################

        # Check if data already was generated:
        self.dlc_cfg["init_weights"] = os.path.join(
            modelfolder, "train", Snapshots[snapshotindex]
        )
        trainingsiterations = (self.dlc_cfg["init_weights"].split(os.sep)[-1]).split("-")[-1]
        # Update number of output and batchsize
        self.dlc_cfg["num_outputs"] = cfg.get("num_outputs", self.dlc_cfg.get("num_outputs", 1))

        self.dlc_cfg["batch_size"] = 1

        if "multi-animal" in self.dlc_cfg["dataset_type"]:
            dynamic = (False, 0.5, 10)  # setting dynamic mode to false
            TFGPUinference = False

        if dynamic[0]:  # state=true
            # (state,detectiontreshold,margin)=dynamic
            print("Starting analysis in dynamic cropping mode with parameters:", dynamic)
            self.dlc_cfg["num_outputs"] = 1
            TFGPUinference = False
            self.dlc_cfg["batch_size"] = 1
            print(
                "Switching batchsize to 1, num_outputs (per animal) to 1 and TFGPUinference to False (all these features are not supported in this mode)."
            )

        # Name for scorer:
        DLCscorer, DLCscorerlegacy = auxiliaryfunctions.GetScorerName(
            cfg,
            1,
            trainFraction,
            trainingsiterations=trainingsiterations,
            modelprefix="",
        )
        if self.dlc_cfg["num_outputs"] > 1:
            if TFGPUinference:
                print(
                    "Switching to numpy-based keypoint extraction code, as multiple point extraction is not supported by TF code currently."
                )
                TFGPUinference = False
            print("Extracting ", self.dlc_cfg["num_outputs"], "instances per bodypart")
            xyz_labs_orig = ["x", "y", "likelihood"]
            suffix = [str(s + 1) for s in range(self.dlc_cfg["num_outputs"])]
            suffix[0] = ""  # first one has empty suffix for backwards compatibility
            xyz_labs = [x + s for s in suffix for x in xyz_labs_orig]
        else:
            xyz_labs = ["x", "y", "likelihood"]

        if TFGPUinference:
            self.sess, self.inputs, self.outputs = predict.setup_GPUpose_prediction(
                self.dlc_cfg, allow_growth=True
            )
        else:
            self.sess, self.inputs, self.outputs = predict.setup_pose_prediction(
                self.dlc_cfg, allow_growth=True
            )

    def get_keypoints(self, frame):
        if not self.is_init:
            self.init_frame(frame)
        frame = img_as_ubyte(frame)
        if frame.shape[-1] == 4:
            frame = rgba2rgb(frame)
        dets = predict_multianimal.predict_batched_peaks_and_costs(
            self.dlc_cfg,
            np.expand_dims(frame, axis=0),
            self.sess,
            self.inputs,
            self.outputs,
        )
        objects = []
        for object in dets:
            positions = object["coordinates"][0]
            probabilities = object["confidence"]
            keypoints = []
            for pos, prob in zip(positions, probabilities):
                if len(pos) == 0:
                    keypoints.append([0, 0, 0])
                elif len(pos) == 1:
                    keypoints.append([pos[0][0], pos[0][1], prob[0][0]])
                else:
                    max_index = np.argmax(prob)
                    keypoints.append([pos[max_index][0], pos[max_index][1], prob[max_index][0]])
            objects.append(keypoints)

        return objects


if __name__ == '__main__':
    tracker = DeepLabCutMA(
        "D:/Informatik/Uni/deeplabcut_tools/projects/gelatin-Dataset-Unity-13-08-38-2022-04-04/config.yaml")
    for i in range(1, 50):
        image = cv2.imread(
            f"D:/Informatik/Uni/deeplabcut_tools/projects/gelatin-Dataset-Unity-13-08-38-2022-04-04/labeled-data/video/image0-{i}.png")
        print(len(tracker.get_keypoints(image)))
