import numpy as np
from scipy.spatial.transform import Rotation

from src.tools.numpy_helper import normalize


def calculate_rvec_error(rvec_orig, rvec_guess):
    return 1 - ((np.dot(normalize(rvec_orig[:, 0]), normalize(rvec_guess[:, 0])) + 1) / 2)


def calculate_tvec_error(tvec_orig, tvec_guess):
    return np.log(np.linalg.norm((tvec_orig[:, 0]) - tvec_guess[:, 0]) + 1)


def calculate_add(rvec_truth, rvec_guess, tvec_truth, tvec_guess, model_points):
    rmatrix_truth = Rotation.from_rotvec(rvec_truth[:, 0]).as_matrix()
    rmatrix_guess = Rotation.from_rotvec(rvec_guess[:, 0]).as_matrix()

    sum = 0

    for model_point in model_points:
        sum += np.linalg.norm(
            ((rmatrix_truth @ model_point + tvec_truth[:, 0]) - (rmatrix_guess @ model_point + tvec_guess[:, 0])))

    return sum / len(model_points)
