import argparse
import os

import numpy as np
import pandas as pd
import scipy.io
from cv2 import cv2
import matplotlib.pyplot as plt
from scipy.integrate import simps
from scipy.spatial.transform import Rotation

from src.detectors.dataset_detector import DatasetDetector
from src.detectors.dope_detector import DopeDetector
from src.detectors.pose_detector import PoseDetector
from src.tools.pose_error import calculate_rvec_error, calculate_tvec_error, calculate_add


def get_arguments():
    parser = argparse.ArgumentParser("Evaluate a model with a YCB-Video Dataset")
    parser.add_argument('model_path', type=str, help="Path to the exported model folder")
    parser.add_argument('object_path', type=str, help="Path to the object data file for the object currently evaluated")
    parser.add_argument('dataset_path', type=str, help="Path to the Linemod Dataset for the object currently evaluated")
    parser.add_argument('dope_path', type=str, help="Path to the weights of the dope model")
    parser.add_argument('ycb_model_name', type=str, help="Name of the YCB-Object")
    return parser.parse_args()


def get_dataset():
    files = list(dict.fromkeys([file.split("-")[0] for file in os.listdir(args.dataset_path)]))

    info = open("{}/{}-box.txt".format(args.dataset_path, files[0]), "r").read()

    objects = [file.split(" ")[0] for file in info.split("\n") if file != ""]
    return files, info, objects


def load_frame(path, file):
    rgb_image = cv2.imread("{}/{}-color.png".format(path, file))
    depth_image = cv2.imread("{}/{}-depth.png".format(path, file))
    label_image = cv2.imread("{}/{}-label.png".format(path, file))
    meta_file = scipy.io.loadmat("{}/{}-meta.mat".format(path, file))

    return rgb_image, depth_image, label_image, meta_file


if __name__ == '__main__':
    args = get_arguments()
    print(args.model_path)
    print(args.object_path)
    print(args.dataset_path)

    dataset_files, dataset_info, dataset_objects = get_dataset()

    print("Objects: {}".format(dataset_objects))

    dist_coeffs = np.zeros((4, 1))

    _, _, _, meta = load_frame(args.dataset_path, dataset_files[0])

    camera_matrix = meta["intrinsic_matrix"]

    # Detectors
    pose_detector = PoseDetector(args.model_path, args.object_path, dist_coeffs, camera_matrix)
    dataset_detector = DatasetDetector(pose_detector.get_model_points(), dist_coeffs, camera_matrix)
    dope_detector = DopeDetector(args.dope_path, dist_coeffs, camera_matrix, args.ycb_model_name)

    model_points = pose_detector.get_model_points()
    model_lines = pose_detector.get_model_lines()

    pose_rvec_error = []
    pose_tvec_error = []

    dope_rvec_error = []
    dope_tvec_error = []

    # Main Loop
    for dataset_file in dataset_files:
        rgb_img, depth_img, label_img, meta = load_frame(args.dataset_path, dataset_file)

        # Dataset
        dataset_rvec, dataset_tvec = dataset_detector.get_pose(meta, dataset_objects.index(args.ycb_model_name))
        print("D:", Rotation.from_rotvec(dataset_rvec[:, 0]).as_quat())
        dataset_solve_img = dataset_detector.draw_model(rgb_img, dataset_rvec, dataset_tvec, model_lines)

        # Pose
        pose_success, pose_rvec, pose_tvec = pose_detector.get_pose(rgb_img)

        if pose_success:
            pose_solve_img = pose_detector.draw_model(rgb_img, pose_rvec, pose_tvec)
            pose_rvec_error.append(calculate_rvec_error(dataset_rvec, pose_rvec))
            pose_tvec_error.append(calculate_tvec_error(dataset_tvec, pose_tvec))
        else:
            pose_solve_img = rgb_img

        # Dope
        dope_success, dope_rvec, dope_tvec = dope_detector.get_pose(rgb_img)
        if dope_success:
            dope_solve_img = dope_detector.draw_model(rgb_img, dope_rvec, dope_tvec, model_points, model_lines)
            dope_rvec_error.append(calculate_rvec_error(dataset_rvec, dope_rvec))
            dope_tvec_error.append(calculate_tvec_error(dataset_tvec, dope_tvec))
        else:
            dope_solve_img = rgb_img
        cv2.imshow("Evaluate Model - RGB", rgb_img)
        cv2.imshow("Evaluate Model - Depth", depth_img)
        cv2.imshow("Evaluate Model - Label", label_img)
        cv2.imshow("Evaluate Model - DatasetSolve", dataset_solve_img)
        cv2.imshow("Evaluate Model - EasyPoseSolve", pose_solve_img)
        cv2.imshow("Evaluate Model - DopeSolve", dope_solve_img)

        c = cv2.waitKey(1)
        if c == 27:
            break

    # Show Errors

    fig, axs = plt.subplots(ncols=2, nrows=2, figsize=(15, 5))
    plt.subplots_adjust(hspace=0.4)
    df_pose_rvec_error = pd.DataFrame(pose_rvec_error)
    df_pose_tvec_error = pd.DataFrame(pose_tvec_error)

    df_dope_rvec_error = pd.DataFrame(dope_rvec_error)
    df_dope_tvec_error = pd.DataFrame(dope_tvec_error)
    axs[0, 0].title.set_text('Rvec Error EasyPose')
    axs[0, 0].plot(df_pose_rvec_error[0], 'lightblue', df_pose_rvec_error[0].rolling(20).mean(), 'b')
    axs[0, 1].title.set_text('Tvec Error EasyPose')
    axs[0, 1].plot(df_pose_tvec_error[0], 'lightblue', df_pose_tvec_error[0].rolling(20).mean(), 'b')

    axs[1, 0].title.set_text('Rvec Error Dope')
    axs[1, 0].plot(df_dope_rvec_error[0], 'lightblue', df_dope_rvec_error[0].rolling(20).mean(), 'b')
    axs[1, 1].title.set_text('Tvec Error Dope')
    axs[1, 1].plot(df_dope_tvec_error[0], 'lightblue', df_dope_tvec_error[0].rolling(20).mean(), 'b')
    plt.show()

    print("EasyPose AUC Rvec:", simps(df_pose_rvec_error[0], dx=5))
    print("EasyPose AUC Tvec:", simps(df_pose_tvec_error[0], dx=5))
    print()
    print("Dope AUC Rvec:", simps(df_dope_rvec_error[0], dx=5))
    print("Dope AUC Tvec:", simps(df_dope_tvec_error[0], dx=5))
