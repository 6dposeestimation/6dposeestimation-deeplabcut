import numpy as np
from cv2 import cv2

from src.live_data import LiveData
from src.filter.moving_average_filter import MovingAverageFilter
from src.tools.draw_3D import draw_model, draw_lines


class PoseDetector:
    def __init__(self, model_path, object_path, dist_coeffs, camera_matrix):
        self.model_points = None
        self.live_data = LiveData(model_path, object_path)

        for i in range(len(self.live_data.all_model_points)):
            self.live_data.all_model_points[i][0] = -self.live_data.all_model_points[i][0]
        self.dist_coeffs = dist_coeffs
        self.camera_matrix = camera_matrix

        self.last_rvec = None
        self.last_tvec = None

        self.rvec_filter = MovingAverageFilter(3, 1)
        self.tvec_filter = MovingAverageFilter(3, 1)

        self.is_first_frame = True

    def get_model_points(self):
        return np.array(self.live_data.all_model_points)

    def get_model_lines(self):
        return self.live_data.lines

    def get_pose(self, img):
        if self.is_first_frame:
            self.live_data.initFrame(img)
            self.is_first_frame = False
        image_points, model_points, image_points_frame, model_points_frame, solve_image = self.live_data.get_keypoints(
            img, None)
        self.model_points = np.array(model_points)
        success, rvec, tvec = self.live_data.get_pose(self.model_points, image_points,
                                                      self.camera_matrix, self.dist_coeffs)

        # Filter
        if self.last_rvec is not None and success:
            error = np.dot(rvec[:, 0], self.last_rvec[:, 0])
            if error > self.rvec_filter.predict()[0] * 2 or error < self.rvec_filter.predict()[0] * 0.5:
                rvec = self.last_rvec
            self.rvec_filter.update([error])

        if self.last_tvec is not None and success:
            error = np.linalg.norm((tvec[:, 0]) - self.last_tvec[:, 0])
            if error > self.tvec_filter.predict()[0] * 2 or error < self.tvec_filter.predict()[0] * 0.5:
                tvec = self.last_tvec
            self.tvec_filter.update([error])

        self.last_rvec = rvec
        self.last_tvec = tvec

        return success, rvec, tvec

    def draw_model(self, img, rvec, tvec):
        img = img.copy()
        projected_model_points, _ = cv2.projectPoints(self.live_data.all_model_points, rvec,
                                                      tvec,
                                                      self.camera_matrix, self.dist_coeffs)
        img = draw_model(img, projected_model_points)
        img = draw_lines(img, projected_model_points, self.live_data.lines)
        return img
