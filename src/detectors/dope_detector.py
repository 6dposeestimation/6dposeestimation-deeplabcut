import json

import numpy as np
import pandas as pd
import pyrr.quaternion
from cv2 import cv2
from pyrr import matrix44, Matrix44, quaternion
from scipy.spatial.transform import Rotation

from src.dope.inference.cuboid import Cuboid3d
from src.dope.inference.cuboid_pnp_solver import CuboidPNPSolver
from src.dope.inference.detector import ModelData, ObjectDetector
from src.tools.draw_3D import draw_model, draw_lines


def quaternion_multiply(quaternion1, quaternion0):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    return np.array([-x1 * x0 - y1 * y0 - z1 * z0 + w1 * w0,
                     x1 * w0 + y1 * z0 - z1 * y0 + w1 * x0,
                     -x1 * z0 + y1 * w0 + z1 * x0 + w1 * y0,
                     x1 * y0 - y1 * x0 + z1 * w0 + w1 * z0], dtype=np.float64)


def right_hand_quat_to_left(quat):
    w, x, y, z = quat
    return np.array([x, y, -z, -w])


class DopeDetector:
    def __init__(self, net_path, dist_coeffs, camera_matrix, name=None):
        self.dist_coeffs = dist_coeffs
        self.camera_matrix = camera_matrix

        self.dope_model = ModelData(name=name, net_path=net_path)
        self.dope_model.load_net_model()
        cuboid = [1, 1, 1]
        if name is not None:
            transform_video2dope = pd.read_csv("src/dope/Transform_video2dope.csv", delimiter=",")
            transform_matrix = np.array(
                transform_video2dope[transform_video2dope.model_name == name].values[0][1:], dtype=float).reshape(
                (4, 4))
            with open("src/dope/ycb_original.json") as f:
                ycb_original = json.load(f)
            index = [i for i, elem in enumerate(ycb_original["exported_object_classes"]) if name in elem][0]
            ycb_object = ycb_original["exported_objects"][index]
            cuboid = ycb_object["cuboid_dimensions"]

            self.base_scale, self.base_rotation, self.base_translation = matrix44.decompose(transform_matrix)
            print(self.base_scale)
            print(self.base_rotation)
            print(self.base_translation)
            self.base_scale, self.base_rotation, self.base_translation = matrix44.decompose(
                np.array(ycb_object["fixed_model_transform"]))

        self.dope_pnp_solver = CuboidPNPSolver(
            self.dope_model,
            camera_matrix,
            Cuboid3d(cuboid),
            dist_coeffs=dist_coeffs)

        self.dope_config = lambda: None
        self.dope_config.mask_edges = 1
        self.dope_config.mask_faces = 1
        self.dope_config.vertex = 1
        self.dope_config.threshold = 0.5
        self.dope_config.softmax = 1000
        self.dope_config.thresh_angle = 0.5
        self.dope_config.thresh_map = 0.01
        self.dope_config.sigma = 3
        self.dope_config.thresh_points = 0.1

    def get_pose(self, img):
        bgr_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        dope_result, _ = ObjectDetector.detect_object_in_image(self.dope_model.net,
                                                               self.dope_pnp_solver,
                                                               bgr_img,
                                                               self.dope_config)
        success = len(dope_result) > 0
        dope_rvec = None
        dope_tvec = None
        if success:
            dope_tvec = np.array(dope_result[0]["location"])
            dope_quat = dope_result[0]["quaternion"]
            if dope_tvec is None or dope_quat is None:
                success = False
            else:

                dope_tvec = np.subtract(dope_tvec, self.base_translation / 10)
                dope_tvec = dope_tvec / 100
                dope_quat = quaternion_multiply(dope_quat, quaternion.inverse(self.base_rotation))
                #dope_quat = quaternion_multiply(self.base_rotation, dope_quat)
                print("Dope:", dope_quat)
                x, w, y, z = dope_quat
                # dope_quat = [-z, -w, x, y]
                # dope_quat = [z, y, -w, -x]
                dope_quat = [w, -z, w, -x]
                print("DopeS:", dope_quat)
                print()
                dope_rvec = Rotation.from_quat(dope_quat).as_rotvec()
                dope_tvec = np.array([[point] for point in dope_tvec])
                dope_rvec = np.array([[point] for point in dope_rvec])

        return success, dope_rvec, dope_tvec

    def draw_model(self, img, rvec, tvec, model_points, lines):
        img = img.copy()
        projected_model_points, _ = cv2.projectPoints(model_points, rvec,
                                                      tvec,
                                                      self.camera_matrix, self.dist_coeffs)
        img = draw_model(img, projected_model_points)
        img = draw_lines(img, projected_model_points, lines)

        return img
