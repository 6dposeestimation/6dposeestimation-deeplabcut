import numpy as np
from cv2 import cv2
from pyrr import matrix33
from scipy.spatial.transform import Rotation

from src.tools.draw_3D import draw_model, draw_lines


class DatasetDetector:
    def __init__(self, model_points, dist_coeffs, camera_matrix):
        self.dist_coeffs = dist_coeffs
        self.camera_matrix = camera_matrix
        self.model_points = model_points

    def get_pose(self, meta_info, model_index):
        poses = meta_info['poses']
        rotation_translation = poses[:, :, model_index]
        rmatrix = rotation_translation[:, :3]

        # Compensate for different coordinate systems
        tvec = rotation_translation[:, 3]
        rvec = Rotation.from_matrix(rmatrix).as_rotvec()

        tvec = np.array([[point] for point in tvec])
        rvec = np.array([[point] for point in rvec])

        return rvec, tvec

    def draw_model(self, img, rvec, tvec, lines):
        img = img.copy()
        projected_model_points, _ = cv2.projectPoints(self.model_points, rvec,
                                                      tvec,
                                                      self.camera_matrix, self.dist_coeffs)
        img = draw_model(img, projected_model_points)
        img = draw_lines(img, projected_model_points, lines)

        return img
