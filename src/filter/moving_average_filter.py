class MovingAverageFilter:
    def __init__(self, steps, values):
        self.values = values
        self.steps = steps
        self.average = [None] * steps
        self.step = 0

    def update(self, values):
        if self.step >= self.steps:
            self.step = 0
        if len(values) != self.values:
            raise ValueError
        self.average[self.step] = values
        self.step += 1

    def predict(self):
        count = 0
        average_value = [0] * self.values
        for value in self.average:
            if value is not None:
                count += 1
                average_value = [x + y for x, y in zip(value, average_value)]
        if count == 0:
            return [0] * self.steps
        return [value / count for value in average_value]
