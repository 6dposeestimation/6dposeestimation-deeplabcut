import argparse
import json
import os
import sys
from datetime import datetime
from shutil import copy

import PIL
import deeplabcut
import pathlib
import os

from ruamel.yaml import YAML


def get_arguments():
    parser = argparse.ArgumentParser("Create and train a model")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-c", '--create', help="Only create the model to train later", action="store_true")
    group.add_argument("-t", '--train', help="Start train a previous created model (Argument: Path to the config file)",
                       type=str)
    group.add_argument("-e", '--export', help="Export a previous trained model (Argument: Path to the config file)",
                       type=str)
    parser.add_argument("-n", '--name', help="Name of the object that gets tracked", type=str)
    parser.add_argument("-l", '--label', help="Create labeled images for dataset", action="store_true")
    parser.add_argument("-m", '--multiobject', help="Dataset is for a multiobject project", action="store_true")
    parser.add_argument("-b", '--batchsize', help="Batch-size used for training", type=int)
    parser.add_argument("-r", '--ready', help="Skip interruption for copying files", action="store_true")
    return parser.parse_args()


class CreateTrainingDataset:
    def __init__(self, scan_object_name=""):
        self.individuals = set()
        self.bodyparts = []
        self.skeleton = []
        self.scorer_list = "scorer"
        self.bodyparts_list = "bodyparts"
        self.individuals_list = "individuals"
        self.coords_list = "coords"
        self.first_file = True
        self.config_file = ""
        if scan_object_name == "":
            print("Please enter the name of the object you want to train")
            self.scan_object_name = input()
        else:
            self.scan_object_name = scan_object_name

    def create_project(self, skip=False, project_name="", multianimal=False):
        if not os.path.isdir("trainData"):
            os.mkdir("trainData")
        if not os.path.isdir("objectData"):
            os.mkdir("objectData")
        if not skip:
            print("Copy train data and object data to the trainData/objectData Folder and press enter to continue")
            input()
        # creates the deeplabcut project
        current_time = datetime.now().strftime("%H-%M-%S")
        if project_name == "":
            self.config_file = deeplabcut.create_new_project('Dataset', 'Unity-' + current_time,
                                                             [pathlib.Path("video.mp4").absolute()],
                                                             working_directory=str(
                                                                 pathlib.Path().absolute()) + "/projects",
                                                             multianimal=multianimal, copy_videos=True)
        else:
            self.config_file = deeplabcut.create_new_project(project_name, 'Dataset-Unity-' + current_time,
                                                             [pathlib.Path("video.mp4").absolute()],
                                                             working_directory=str(
                                                                 pathlib.Path().absolute()) + "/projects",
                                                             multianimal=multianimal, copy_videos=True)

    def create_dataset(self):
        # opens and loads the config file for deeplabcut
        picture_list = []
        print(pathlib.Path(self.config_file))
        file = open(pathlib.Path(self.config_file), "r")
        cfg = YAML().load(file)

        file = open(pathlib.Path("objectData/" + self.scan_object_name + ".json"), "r")
        data = json.load(file)
        for keypoint in data["keypoints"]:
            self.bodyparts.append(keypoint["name"])
        for bone in data["skeleton"]:
            self.skeleton.append((bone["keypoint1"], bone["keypoint2"]))
        for data_name in os.listdir("trainData"):
            # break loop if there are no more data files
            if "image" in data_name:
                continue

            # open data and image file
            file = open(pathlib.Path("trainData/" + data_name), "r")
            data = json.load(file)
            image_name = "image" + data_name.split("data")[1].split(".")[0] + ".png"
            image = PIL.Image.open("trainData/" + image_name)

            # read size of image
            image_width, image_height = image.size

            for item in data["Items"]:
                # search for the scan object item
                if item["name"] == self.scan_object_name:
                    # create header for the csv file if its the first file
                    if self.first_file:
                        self.first_file = False
                        for i, part in enumerate(item["keyPointScreenCoordinates"]):
                            self.bodyparts_list += "," + self.bodyparts[i]
                            self.bodyparts_list += "," + self.bodyparts[i]
                            self.scorer_list += "," + cfg["scorer"]
                            self.scorer_list += "," + cfg["scorer"]
                            self.coords_list += ",x"
                            self.coords_list += ",y"

                    # data for one frame of the training set
                    image_data = "labeled-data/video/" + image_name

                    # scan all key points and map them to the real image coordinates if the keypoint is visible else -> skip
                    for coord in item["keyPointScreenCoordinates"]:
                        if coord["z"] == 1:
                            image_data += "," + str(coord["x"] * image_width) + "," + str(
                                image_height - coord["y"] * image_height)
                        else:
                            image_data += ",,"
                    image_data += "\n"

                    # save image_data
                    picture_list.append(image_data)
                    # copy image to the training folder
                    copy("trainData/" + image_name,
                         cfg["project_path"] + "/labeled-data/video")
                    break
            file.close()
        # test if training data is found
        if self.first_file:
            print("No key points found for the object!")
            sys.exit()

        # open file for training data
        file = open(cfg["project_path"] + "/labeled-data/video/CollectedData_" + cfg["scorer"] + ".csv", "w")

        # store training data
        file.writelines(
            [self.scorer_list, "\n", self.bodyparts_list, "\n", self.coords_list, "\n", ] + picture_list)
        file.close()

        # edit config for new bodyparts and save
        cfg["bodyparts"] = self.bodyparts
        cfg["skeleton"] = self.skeleton
        file = open(pathlib.Path(self.config_file), "w")
        YAML().dump(cfg, file)

        # convert training data file to h5
        deeplabcut.convertcsv2h5(self.config_file, False)

    def create_multianimal_dataset(self):
        # opens and loads the config file for deeplabcut
        picture_list = {}
        print(pathlib.Path(self.config_file))
        file = open(pathlib.Path(self.config_file), "r")
        cfg = YAML().load(file)

        file = open(pathlib.Path("objectData/" + self.scan_object_name + ".json"), "r")
        data = json.load(file)
        for keypoint in data["keypoints"]:
            self.bodyparts.append(keypoint["name"])
        for bone in data["skeleton"]:
            self.skeleton.append((bone["keypoint1"], bone["keypoint2"]))
        index_most_individuals = 0
        count_individuals = 0
        for i, data_name in enumerate(os.listdir("trainData")):
            # break loop if there are no more data files
            if "image" in data_name:
                continue

            # open data and image file
            file = open(pathlib.Path("trainData/" + data_name), "r")
            data = json.load(file)
            count = 0
            for item in data["Items"]:
                if self.scan_object_name in item["name"]:
                    count += 1
            if count > count_individuals:
                count_individuals = count
                index_most_individuals = i
        for frame_index, data_name in enumerate(os.listdir("trainData")):
            # break loop if there are no more data files
            if "image" in data_name:
                continue

            # open data and image file
            file = open(pathlib.Path("trainData/" + data_name), "r")
            data = json.load(file)
            image_name = "image" + data_name.split("data")[1].split(".")[0] + ".png"
            image = PIL.Image.open("trainData/" + image_name)

            # read size of image
            image_width, image_height = image.size
            for item in data["Items"]:
                # search for the scan object item
                if self.scan_object_name in item["name"]:
                    # create header for the csv file if its the first file
                    self.individuals.add(item["name"])
                    if frame_index == index_most_individuals:
                        for i, part in enumerate(item["keyPointScreenCoordinates"]):
                            self.bodyparts_list += "," + self.bodyparts[i]
                            self.bodyparts_list += "," + self.bodyparts[i]
                            self.individuals_list += "," + item["name"]
                            self.individuals_list += "," + item["name"]
                            self.scorer_list += "," + cfg["scorer"]
                            self.scorer_list += "," + cfg["scorer"]
                            self.coords_list += ",x"
                            self.coords_list += ",y"

                    # data for one frame of the training set
                    if image_name not in picture_list:
                        picture_list[image_name] = "labeled-data/video/" + image_name
                    image_data = ""

                    # scan all key points and map them to the real image coordinates if the keypoint is visible else -> skip
                    for coord in item["keyPointScreenCoordinates"]:
                        if coord["z"] == 1:
                            image_data += "," + str(coord["x"] * image_width) + "," + str(
                                image_height - coord["y"] * image_height)
                        else:
                            image_data += ",,"

                    # save image_data
                    picture_list[image_name] += image_data
                    # copy image to the training folder
                    copy("trainData/" + image_name,
                         cfg["project_path"] + "/labeled-data/video")
            file.close()
        # test if training data is found
        expected_fields = len(self.individuals) * len(self.bodyparts) * 2
        for picture_path in picture_list:
            field_count = picture_list[picture_path].count(",")
            picture_list[picture_path] += "," * (expected_fields - field_count)
        if count_individuals == 0:
            print("No key points found for the object!")
            sys.exit()

        # open file for training data
        file = open(cfg["project_path"] + "/labeled-data/video/CollectedData_" + cfg["scorer"] + ".csv", "w")

        # store training data
        file.writelines(
            [self.scorer_list, "\n", self.individuals_list, "\n", self.bodyparts_list, "\n", self.coords_list,
             "\n", "\n".join(picture_list.values())])
        file.close()

        # edit config for new bodyparts and save
        cfg["multianimalbodyparts"] = self.bodyparts
        cfg["bodyparts"] = "MULTI!"
        cfg["individuals"] = list(self.individuals)
        cfg["skeleton"] = self.skeleton
        file = open(pathlib.Path(self.config_file), "w")
        YAML().dump(cfg, file)

        # convert training data file to h5
        deeplabcut.convertcsv2h5(self.config_file, False)

    def label_images(self, user_input=True):
        if user_input:
            print("Do you want to create labeled images to check the key points(Y/N, default=N)")
            if "y" in input().lower():
                # create images with labels
                deeplabcut.check_labels(self.config_file)
        else:
            deeplabcut.check_labels(self.config_file)

    def create_training_dataset(self):
        # create training dataset
        deeplabcut.create_training_dataset(self.config_file)

    def create_multianimal_training_dataset(self):
        # create training dataset
        deeplabcut.create_multianimaltraining_dataset(self.config_file)

    def start_training(self, batchsize):
        if not batchsize:
            batchsize = 8
        file = open(pathlib.Path(self.config_file), "r")
        cfg = YAML().load(file)
        file.close()
        cfg["batch_size"] = batchsize
        file = open(pathlib.Path(self.config_file), "w")
        YAML().dump(cfg, file)
        file.close()
        try:
            deeplabcut.train_network(self.config_file, displayiters=100, saveiters=1000, allow_growth=True)
        except KeyboardInterrupt:
            print("Stopped Training")

    def export_model(self):
        deeplabcut.export_model(self.config_file)


if __name__ == '__main__':
    args = get_arguments()
    if not args.name:
        name = ""
    else:
        name = args.name
    if args.multiobject:
        multianimal = True
    else:
        multianimal = False
    if args.ready:
        skip = True
    else:
        skip = False
    if args.create:
        dataset = CreateTrainingDataset(name)
        dataset.create_project(skip=skip, project_name=name, multianimal=multianimal)
        if multianimal:
            dataset.create_multianimal_dataset()
        else:
            dataset.create_dataset()
        if args.label:
            dataset.label_images(False)
        dataset.create_training_dataset()
    elif args.train:
        dataset = CreateTrainingDataset(" ")
        dataset.config_file = os.path.abspath(args.train)
        dataset.start_training(args.batchsize)
        dataset.export_model()
    elif args.export:
        dataset = CreateTrainingDataset(" ")
        dataset.config_file = os.path.abspath(args.export)
        dataset.export_model()
    else:
        dataset = CreateTrainingDataset(name)
        dataset.create_project(skip=skip, project_name=name, multianimal=multianimal)
        if multianimal:
            dataset.create_multianimal_dataset()
        else:
            dataset.create_dataset()
        if args.label:
            dataset.label_images(False)
        dataset.create_training_dataset()
        dataset.start_training(args.batchsize)
        dataset.export_model()
